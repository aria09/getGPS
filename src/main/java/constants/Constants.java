package constants;

import java.util.Arrays;
import java.util.List;

public enum Constants {
    APP_ID("jDKoQ3FGyFKRxgFVM7tk"),
    APP_CODE("xHZtmA15ChGbZgIwsPVOQg"),
    GEN("8"),
    ACTION("run", "cancel", "status");

//    public static final String APP_ID = "jDKoQ3FGyFKRxgFVM7tk";
//    public static final String APP_CODE = "xHZtmA15ChGbZgIwsPVOQg";
//    public static final String GEN = "8";
//    public static final String ACTION = ("run", "");

    private List<String> value;

    Constants(String... value) {
        this.value = Arrays.asList(value);
    }

    public List<String> getValue() {
        return value;
    }
}
