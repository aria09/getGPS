package main;


import controller.MainController;
import controller.ScreensController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Main extends Application {

    public static ScreensController mainContainer;
    public static Stage aStage;
    private static String mainVew = "Main";
    private static String mainFile = "/view/Main.fxml";


    public static void main(String[] args) throws IOException {

        launch(args);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println();
            System.out.println("Exit");
        }));
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        final PosWindow dragWin = new PosWindow();
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("LẤY TỌA ĐỘ GPS");
        primaryStage.centerOnScreen();
        aStage = primaryStage;


        mainContainer = new ScreensController();
        mainContainer.loadScreen(Main.mainVew, Main.mainFile);
        mainContainer.setScreen(Main.mainVew);

        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);

        root.setOnMousePressed(mouseEvent -> {
            dragWin.x = primaryStage.getX() - mouseEvent.getScreenX();
            dragWin.y = primaryStage.getY() - mouseEvent.getScreenY();
        });

        root.setOnMouseDragged(mouseEvent -> {
            primaryStage.setX(mouseEvent.getScreenX() + dragWin.x);
            primaryStage.setY(mouseEvent.getScreenY() + dragWin.y);
        });

        scene.getStylesheets().add(getClass().getResource("/view/css/style.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

        MainController mc = new MainController();
        mc.start();
    }

    private class PosWindow {
        double x, y;
    }

}
