package ulti;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;

public class XMLReader {

    public String getXMLString(String xmlString, String xmlPath) {
        String result = "";

        try {
            Document doc = stringToDom(xmlString);
            result = getString(doc, xmlPath);
        } catch (SAXException | ParserConfigurationException | XPathExpressionException | IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private Document stringToDom(String xmlSource)
            throws SAXException, ParserConfigurationException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlSource)));
    }

    private String getString(Document document, String xmlPath) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        Node node = (Node) xPath.compile(xmlPath).evaluate(document, XPathConstants.NODE);
        return node.getTextContent();
    }
}
