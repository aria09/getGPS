package ulti;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelWriter {

    private XSSFWorkbook workbook;
    private Sheet sheet;
    private XSSFCellStyle style;
    private XSSFFont font;

    public void createExcel() {
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("GPS");
        style = workbook.createCellStyle();
        font = workbook.createFont();
        style.setFillForegroundColor(new XSSFColor(new java.awt.Color(180, 198, 231)));
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER);
        font.setBold(true);
        style.setFont(font);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
    }

    public void closeExcel() {
        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(List<String> dataList, int countLine, String location) throws IOException {

//        location = location
//                .replaceAll("%", "%25")
//                .replaceAll(" ", "%20")
//                .replaceAll("!", "%21")
//                .replaceAll("@", "%40")
//                .replaceAll("#", "%23")
//                .replaceAll("\\$", "%24")
//                .replaceAll("\\^", "%5E")
//                .replaceAll("&", "%26")
//                .replaceAll("\\*", "%2A")
//                .replaceAll("\\(", "%28")
//                .replaceAll("\\)", "%29")
//                .replaceAll("\\{", "%7B")
//                .replaceAll("}", "%7D")
//                .replaceAll("~", "%7E")
//                .replaceAll("`", "%60")
//                .replaceAll(":", "%3A")
//                .replaceAll("\\.", "%2E")
//                .replaceAll(",", "%2C")
//                .replaceAll("\\+", "%2B")
//                .replaceAll("=", "%3D")
//                .replaceAll("@", "%40");


        // Write Data GPS
        Row row = sheet.createRow(countLine);
        int index = 0;
        for (String line : dataList) {
            Cell cell = row.createCell(index++);
            cell.setCellValue(line);
            if (countLine == 0) cell.setCellStyle(style);
        }

        try (FileOutputStream outputStream = new FileOutputStream(location + "DATA GPS.xlsx")) {
            workbook.write(outputStream);
            outputStream.flush();
        }
//        workbook.close();
    }


}
