package ulti;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipInputStreamReader {

    public void getZipData(InputStream input, String location) throws IOException {
        ExcelWriter excelWriter = new ExcelWriter();
        excelWriter.createExcel();

        // create a buffer to improve copy performance later.
        byte[] buffer = new byte[2048];

        // open the zip file stream
        ZipInputStream stream = new ZipInputStream(input, Charset.forName("UTF-8"));

        try {
            // now iterate through each item in the stream. The get next
            // entry call will return a ZipEntry for each file in the
            // stream
            ZipEntry entry;
            while ((entry = stream.getNextEntry()) != null) {
                String s = String.format("Entry: %s len %d added %TD",
                        entry.getName(), entry.getSize(),
                        new Date(entry.getTime()));
                System.out.println(s);

                // Once we get the entry from the stream, the stream is
                // positioned read to read the raw data, and we keep
                // reading until read returns 0 or less.

                Scanner sc = new Scanner(stream, "UTF-8");
                int countLine = 0;
                while (sc.hasNextLine()) {
                    excelWriter.write(Arrays.asList(sc.nextLine().split("\\|")), countLine, location);
                    countLine++;
                }
            }
        } finally {
            // we must always close the zip file.
            stream.close();
            excelWriter.closeExcel();
        }
    }
}
