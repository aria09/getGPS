package ulti;

import model.DataAddress;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelReader {

    public List<DataAddress> readExcel(String location) {
        DataAddress aData;
        List<DataAddress> allData = new ArrayList<>();

        Workbook workbook = checkFileType(location);
        Sheet dataTypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = dataTypeSheet.iterator();

        while (iterator.hasNext()) {
            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            if (currentRow.getRowNum() >= 1) {
                aData = new DataAddress();

                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    int indexCell = currentCell.getColumnIndex();

                    if (getDataCell(dataTypeSheet, 0, indexCell).getStringCellValue().equalsIgnoreCase("recId")) {
                        currentCell.setCellType(CellType.STRING);
                        aData.setRecId(currentCell.getStringCellValue());
                    } else if (getDataCell(dataTypeSheet, 0, indexCell).getStringCellValue().equalsIgnoreCase("searchText")) {
                        currentCell.setCellType(CellType.STRING);
                        aData.setSearchText(currentCell.getStringCellValue());
                    }
                }
                allData.add(aData);
            }
        }
        return allData;
    }


    private Workbook checkFileType(String FILE_NAME) {
        Workbook workbook = null;
        FileInputStream excelFile;

        try {
            excelFile = new FileInputStream(new File(FILE_NAME));

            if (FILE_NAME.toLowerCase().endsWith("xlsx")) {
                workbook = new XSSFWorkbook(excelFile);
            } else if (FILE_NAME.toLowerCase().endsWith("xls")) {
                workbook = new HSSFWorkbook(excelFile);
            } else {
                throw new IllegalArgumentException("Wrong Excel file");
            }

        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File cannot found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return workbook;
    }


    private boolean isSheetEmpty(Sheet sheet) {
        for (Row row : sheet) {
            Iterator<Cell> cells = row.cellIterator();
            while (cells.hasNext()) {
                Cell cell = cells.next();
                cell.setCellType(CellType.STRING);
                if (!cell.getStringCellValue().isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    private Cell getDataCell(Sheet sheet, int row, int column) {
        return sheet.getRow(row).getCell(column, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
    }

}
