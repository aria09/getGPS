package logic;

import constants.Constants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import ulti.XMLReader;
import ulti.ZipInputStreamReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

public class ApacheHttpClient {

    private CloseableHttpClient httpClient;

    public void getConnect() {
        httpClient = HttpClientBuilder.create().build();
    }

    public void closeConnect() throws IOException {
        httpClient.close();
    }

    public String postData(String sData, String email) throws IOException, URISyntaxException {

        URIBuilder builder = new URIBuilder();
        builder.setScheme("https").setHost("batch.geocoder.api.here.com").setPath("/6.2/jobs")
                .setParameter("app_id", Constants.APP_ID.getValue().get(0))
                .setParameter("app_code", Constants.APP_CODE.getValue().get(0))
                .setParameter("gen", Constants.GEN.getValue().get(0))
                .setParameter("action", Constants.ACTION.getValue().get(0))
                .setParameter("mailto", email)
                .setParameter("header", "true")
                .setParameter("indelim", "|")
                .setParameter("outdelim", "|")
                .setParameter("outcols", "displayLatitude,displayLongitude,locationLabel,houseNumber,street,district,city,county,state,country")
                .setParameter("outputCombined", "true");

        URI uri = builder.build();

        System.out.println(uri);
        HttpPost postRequest = new HttpPost(uri);

        postRequest.setHeader("Content-type", "text/plain;charset=UTF-8");

        StringEntity entity = new StringEntity("recId|searchText|country\n" + sData, "UTF-8");
        System.out.println("recId|searchText|country\n" + sData);
        postRequest.setEntity(entity);

        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        System.out.println("Output from Server .... \n");
//        System.out.println(getResult(response, "//Response/MetaInfo/RequestId"));
        return getResult(response, "//Response/MetaInfo/RequestId");
    }


    public Boolean getData(String sID, String filePath) throws IOException, UnsupportedOperationException, URISyntaxException {

        URIBuilder builder = new URIBuilder();
        builder.setScheme("https").setHost("batch.geocoder.api.here.com")
                .setPath("/6.2/jobs/" + sID + "/result")
                .setParameter("app_id", Constants.APP_ID.getValue().get(0))
                .setParameter("app_code", Constants.APP_CODE.getValue().get(0));

        URI uri = builder.build();
        HttpGet request = new HttpGet(uri);

        HttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();

        int responseCode = response.getStatusLine().getStatusCode();

        System.out.println("Request Url: " + request.getURI());
        System.out.println("Response Code: " + responseCode);

        if (responseCode != 200) return false;
        InputStream is = entity.getContent();

        ZipInputStreamReader zipInputStreamReader = new ZipInputStreamReader();
        zipInputStreamReader.getZipData(is, filePath);

        is.close();
        System.out.println("File Download Completed!!!");
        return true;
    }

    private String getResult(HttpResponse response, String xmlPath) {
//        "//Response/MetaInfo/RequestId"
        String result = "";
        String output;
        StringBuilder dataReceived = new StringBuilder();
        XMLReader xmlReader = new XMLReader();

        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            while ((output = br.readLine()) != null) {
                dataReceived.append(output);
            }

            result = xmlReader.getXMLString(dataReceived.toString(), xmlPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
