package controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import logic.ApacheHttpClient;
import main.Main;
import model.DataAddress;
import ulti.ExcelReader;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MainController implements Initializable, ControlledScreen {

    ScreensController myController;

    @FXML
    private Button minButton;
    @FXML
    private Button closeButton;
    @FXML
    private Button browserButton;
    @FXML
    private TextField fileLocation;
    @FXML
    private TextField email;
    @FXML
    private Button sendButton;
    @FXML
    private Button saveButton;
    @FXML
    private TextField textID;
    @FXML
    private ProgressIndicator progressIndicator;

    private FileChooser fileChooser;
    private DirectoryChooser directoryChooser;
    private File file;
    private File saveFile;
    private String filePath;
    private String preDir;
    private ApacheHttpClient client;
    private boolean checkResult;

    private static void configureFileChooser(FileChooser fileChooser) {
        fileChooser.setTitle("Load Excel Data");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel File", "*.xlsx", "*.xls")
        );
    }

    private static void configuringDirectoryChooser(DirectoryChooser directoryChooser) {
        directoryChooser.setTitle("Lưu file GPS");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    }

    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        email.setText("Van.NguyenThiBich@kantartns.com");

        client = new ApacheHttpClient();

        fileChooser = new FileChooser();
        directoryChooser = new DirectoryChooser();

        progressIndicator = new ProgressIndicator();
        closeButton.setOnAction(actionEvent -> Platform.exit());
        minButton.setOnAction(actionEvent -> Main.aStage.setIconified(true));
        browserButton.setOnAction(actionEvent -> {
            configureFileChooser(fileChooser);

            if (preDir != null) fileChooser.setInitialDirectory(new File(preDir));

            file = fileChooser.showOpenDialog(Main.aStage);

            if (file != null) {
                fileLocation.setText(file.getAbsolutePath());
                filePath = file.getAbsolutePath();
                preDir = file.getParent();
            }
        });

        sendButton.setOnAction(actionEvent -> {
            if (filePath == null || filePath.length() <= 0) {
                if (Platform.isFxApplicationThread()) {
                    dialog("Vui lòng chọn file địa chỉ").run();
                } else {
                    Platform.runLater(dialog(null));
                }
            } else if (email.getText().length() <= 0) {
                if (Platform.isFxApplicationThread()) {
                    dialog("Vui lòng nhập địa chỉ email").run();
                } else {
                    Platform.runLater(dialog(null));
                }
            } else if (email.getText().length() > 0 & !email.getText().matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")) {
                if (Platform.isFxApplicationThread()) {
                    dialog("Vui lòng kiểm tra lại địa chỉ email").run();
                } else {
                    Platform.runLater(dialog(null));
                }
            } else {
                if (Platform.isFxApplicationThread()) {
                    dialogInfo(sendInfo(filePath)).run();
                } else {
                    Platform.runLater(dialog(null));
                }
            }
        });

        saveButton.setOnAction(actionEvent ->
        {
            configuringDirectoryChooser(directoryChooser);
            checkResult = false;

            if (textID.getText().length() <= 0) {
                if (Platform.isFxApplicationThread()) {
                    dialog("Vui lòng nhập ID download").run();
                } else {
                    Platform.runLater(dialog(null));
                }
            } else {
                try {
//                    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
                    saveFile = directoryChooser.showDialog(Main.aStage);
                    client.getConnect();

                    if (saveFile == null) return;

                    Runnable getData = () -> {
                        try {
                            checkResult = client.getData(textID.getText(), saveFile.getAbsolutePath() + "\\");
                        } catch (IOException | URISyntaxException e) {
                            e.printStackTrace();
                        }
                    };

                    if (Platform.isFxApplicationThread()) {
                        progressIndicator.setVisible(true);
//                        Future<?> future = scheduler.submit(getData);
//                        scheduler.submit(dialog("Please wait for saving data"));
//                        dialog("Please wait for saving data").run();
                        getData.run();
//                        try {
//                            future.get();
//                        } catch (InterruptedException | ExecutionException e) {
//                            e.printStackTrace();
//                        }
                    } else {
//                        Platform.runLater(dialog(null));
                        Platform.runLater(getData);
                    }

                    if (checkResult) dialog("Data đã được lưu").run();
                    else if (!checkResult) dialog("Vui lòng thử lại sau").run();

                    client.closeConnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String sendInfo(String filePath) {
        String result = "";
        ExcelReader excelReader = new ExcelReader();
        List<DataAddress> allData = excelReader.readExcel(filePath);
        String combine = allData.stream().map(aData -> aData.getRecId() + "|" + aData.getSearchText() + "|" + aData.getCountry()).collect(Collectors.joining("\n"));

        try {
            client.getConnect();
            result = client.postData(combine, email.getText());
            client.closeConnect();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Runnable dialog(String msg) {
        return () -> {
            Alert dlg = new Alert(Alert.AlertType.INFORMATION);
            dlg.initModality(Modality.NONE);
            dlg.setHeaderText(null);
            dlg.setContentText(msg);
            dlg.initStyle(Main.aStage.getStyle());

            DialogPane dialogPane = dlg.getDialogPane();
            dialogPane.getStylesheets().add(getClass().getResource("/view/css/style.css").toExternalForm());
            dialogPane.getStyleClass().add("dialog-pane");

            dlg.show();
        };
    }

    private Runnable dialogInfo(String msg) {
        return () -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initModality(Modality.NONE);
            alert.setHeaderText(null);
            alert.setContentText(null);

            Label label = new Label("ID để download data GPS:");

            TextArea textArea = new TextArea(msg);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            DialogPane dialogPane = alert.getDialogPane();
            dialogPane.getStylesheets().add(getClass().getResource("/view/css/style.css").toExternalForm());
            dialogPane.getStyleClass().add("dialog-pane");
            dialogPane.setExpandableContent(expContent);
            dialogPane.setExpanded(true);

            alert.show();
        };
    }

    public void start() {
        System.out.println("Running...");
    }

    @Override
    public void setScreenParent(ScreensController screenPage) {
        myController = screenPage;
    }

}

