package model;

public class DataAddress {

    private String recId;
    private String searchText;
    private String country;

    public DataAddress() {
        country = "VNM";
    }


    public String getRecId() {
        return recId;
    }

    public void setRecId(String recId) {
        this.recId = recId.trim();
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText.replaceAll("\\(.*\\)", "").trim();
    }

    public String getCountry() {
        return country;
    }

}
